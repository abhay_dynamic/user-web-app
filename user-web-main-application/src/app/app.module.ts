import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GroupService } from './group.service';
import { HomeComponent } from './home/home.component';
import { NbCardModule, NbChatModule, NbIconModule, NbLayoutModule, NbMenuModule, NbSearchModule, NbSelectModule, NbSidebarModule, NbThemeModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { App1SharedModule }
//   from "../../../member/src/app/app.module";
import { ChatAppModule }
  from "../../../user-web-header-application/src/app/app.module";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ], 
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    // App1SharedModule,
    ChatAppModule,
    NbThemeModule.forRoot({ name: 'corporate' }),
    NbCardModule,
    NbIconModule,               // <---------
    NbSidebarModule.forRoot(),  // <---------
    NbMenuModule.forRoot(),     // <---------
    NbLayoutModule,
    NbEvaIconsModule,
    NbSearchModule,
    NbSelectModule,
    NbChatModule,
    BrowserAnimationsModule,
  ],
  providers: [GroupService],
  bootstrap: [AppComponent]
})
export class AppModule { }
