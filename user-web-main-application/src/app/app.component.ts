import { loadRemoteModule } from '@angular-architects/module-federation';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, Injector, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NbMenuItem, NbSidebarService, NbThemeService } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'shell';
  selectedItem: string = "corporate";
  constructor(private readonly sidebarService: NbSidebarService,
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private changeDetectorRef: ChangeDetectorRef,
    private themeService: NbThemeService
  ) {

  }

  messages: any[] = [
    {
      text: 'Custom template was provided as a title!',
      date: new Date(),
      reply: false,
      user: {
        name: 'Bot',
        avatar: 'https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/robot-face.png',
      },
    },
  ];

  sendMessage(event: any, userName: string, avatar: string, reply: boolean) {
    const files = !event.files ? [] : event.files.map((file: { src: any; type: any; }) => {
      return {
        url: file.src,
        type: file.type,
        icon: 'file-text-outline',
      };
    });

    this.messages.push({
      text: event.message,
      date: new Date(),
      reply: reply,
      type: files.length ? 'file' : 'text',
      files: files,
      user: {
        name: userName,
        avatar: avatar,
      },
    });
  }

  changeTheme(data: string) {

    this.themeService.changeTheme(data);
 

  }

  isProfile: boolean = true;
  items: NbMenuItem[] = [
    {
      title: 'Home',
      icon: 'home-outline',
      link: '/home',
      home: true
    },
    {
      title: 'Users',
      icon: 'people-outline',
      link: '/users'
    }
  ];

  memberFactoy: any;
  chatFactoy: any;
  profileFactorry: any;
  feedFactorry: any

  ngOnInit(): void {

  }



  @ViewChild('user', { read: ViewContainerRef, static: true })
  headerContainer!: ViewContainerRef;

  @ViewChild('member', { read: ViewContainerRef, static: true })
  MemberContainer!: ViewContainerRef;

  @ViewChild('chat', { read: ViewContainerRef, static: true })
  ChatContainer!: ViewContainerRef;

  @ViewChild('member2', { read: ViewContainerRef, static: true })
  Member2Container!: ViewContainerRef;

  @ViewChild('feed', { read: ViewContainerRef, static: true })
  FeedContainer!: ViewContainerRef;

  @ViewChild('calender', { read: ViewContainerRef, static: true })
  CalenderContainer!: ViewContainerRef;

  @ViewChild('profile', { read: ViewContainerRef, static: true })
  ProfileContainer!: ViewContainerRef;

  // ngAfterViewInit(): void {
  //   // load header
  //   loadRemoteModule({
  //     remoteEntry: 'http://localhost:3000/remoteEntry.js',
  //     remoteName: 'mfe1',
  //     exposedModule: 'UserComponent',
  //   }).then(module => {
  //     const factory = this.resolver.resolveComponentFactory(module.UserComponent);
  //     this.headerContainer?.createComponent(factory, undefined, this.injector);
  //   });

  //   // loadRemoteModule({
  //   //   remoteEntry: 'http://localhost:7000/remoteEntry.js',
  //   //   remoteName: 'mfe2',
  //   //   exposedModule: 'MicrofrontendcreateComponent',
  //   // }).then(module => {
  //   //   this.memberFactoy = this.resolver.resolveComponentFactory(module.MicrofrontendcreateComponent);
  //   //   this.MemberContainer?.createComponent(this.memberFactoy, undefined, this.injector);
  //   // });
  //   loadRemoteModule({
  //     remoteEntry: 'http://localhost:7000/remoteEntry.js',
  //     remoteName: 'mfe2',
  //     exposedModule: 'Microfrontend1Component',
  //   }).then(module => {
  //     this.profileFactorry = this.resolver.resolveComponentFactory(module.Microfrontend1Component);
  //     this.ProfileContainer?.createComponent(this.profileFactorry, undefined, this.injector);
  //   });
  //   loadRemoteModule({
  //     remoteEntry: 'http://localhost:4300/remoteEntry.js',
  //     remoteName: 'chat',
  //     exposedModule: './User',
  //   }).then(module => {
  //     console.log(module);
      
  //     // this.chatFactoy = this.resolver.resolveComponentFactory(module.UserComponent);
  //     // this.ChatContainer?.createComponent(this.chatFactoy, undefined, this.injector);
  //   });

  //   // loadRemoteModule({
  //   //   remoteEntry: 'http://localhost:4200/remoteEntry.js',
  //   //   remoteName: 'calender',
  //   //   exposedModule: 'CalenderComponent',
  //   // }).then(module => {
  //   //   const factory = this.resolver.resolveComponentFactory(module.CalenderComponent);
  //   //   this.CalenderContainer?.createComponent(factory, undefined, this.injector);
  //   // });

  //   loadRemoteModule({
  //     remoteEntry: 'http://localhost:9000/remoteEntry.js',
  //     remoteName: 'mfe3',
  //     exposedModule: 'UserComponent',
  //   }).then(module => {
  //     this.feedFactorry = this.resolver.resolveComponentFactory(module.UserComponent);
  //     this.FeedContainer?.createComponent(this.feedFactorry, undefined, this.injector);
  //   });
  // }

  toggleSidebar(): boolean {
    this.sidebarService.toggle();
    return false;
  }
  onClick() {
    this.isProfile = !this.isProfile;
    this.isProfile && this.attachComponent();

    !this.isProfile && this.removeComponent();
    this.changeDetectorRef.detectChanges();
  }
  private attachComponent() {
    this.Member2Container.clear();
    this.MemberContainer?.createComponent(this.memberFactoy, undefined, this.injector);

  }

  private removeComponent() {
    this.MemberContainer.clear();
    this.Member2Container?.createComponent(this.feedFactorry, undefined, this.injector);
  }

}


