import {ErrorHandler, Injectable} from '@angular/core';
import {LoggerService} from './some/logger.service';

@Injectable()
export class CustomErrorHandlerService extends ErrorHandler {

    constructor(private logger: LoggerService) {
        super();
    }

    handleError(error) {
        // Here you can provide whatever logging you want
        this.logger.logError(error);
        super.handleError(error);
    }
}