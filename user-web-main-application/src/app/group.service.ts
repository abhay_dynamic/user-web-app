import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
export class GroupService {

    constructor(private http: HttpClient) { 
    }

    public getJSON(): Observable<any> {
        return this.http.get("http://localhost:5000/assets/group.json");
    }

    getDetails(id: any) {
        return this.http.get("http://localhost:5000/assets/group.json")
            .pipe(map((response: any) => {
                return response.find((item: any) => (item.groupId === id));
            }));
        }
}