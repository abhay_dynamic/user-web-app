import { loadRemoteModule } from '@angular-architects/module-federation';
import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, Injector, OnInit, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit, OnInit {
  isProfile: boolean = true;
  memberFactoy: any;
  feedFactorry: any; 


  constructor(
    private injector: Injector,
    private resolver: ComponentFactoryResolver,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {

  }

  @ViewChild('user', { read: ViewContainerRef, static: true })
  headerContainer!: ViewContainerRef;

  @ViewChild('member', { read: ViewContainerRef, static: true })
  MemberContainer!: ViewContainerRef;

  

  @ViewChild('member2', { read: ViewContainerRef, static: true })
  Member2Container!: ViewContainerRef;

  @ViewChild('feed', { read: ViewContainerRef, static: true })
  FeedContainer!: ViewContainerRef;

  @ViewChild('calender', { read: ViewContainerRef, static: true })
  CalenderContainer!: ViewContainerRef;


  ngAfterViewInit(): void {
    // load header
    loadRemoteModule({
      remoteEntry: 'http://localhost:3000/remoteEntry.js',
      remoteName: 'mfe1',
      exposedModule: 'UserComponent',
    }).then(module => {
      const factory = this.resolver.resolveComponentFactory(module.UserComponent);
      this.headerContainer?.createComponent(factory, undefined, this.injector);
    });

    loadRemoteModule({
      remoteEntry: 'http://localhost:7000/remoteEntry.js',
      remoteName: 'mfe2',
      exposedModule: 'MicrofrontendcreateComponent',
    }).then(module => {
      this.memberFactoy = this.resolver.resolveComponentFactory(module.MicrofrontendcreateComponent);
      this.MemberContainer?.createComponent(this.memberFactoy, undefined, this.injector);
    }); 

    // loadRemoteModule({
    //   remoteEntry: 'http://localhost:4200/remoteEntry.js',
    //   remoteName: 'calender',
    //   exposedModule: 'CalenderComponent',
    // }).then(module => {
    //   const factory = this.resolver.resolveComponentFactory(module.CalenderComponent);
    //   this.CalenderContainer?.createComponent(factory, undefined, this.injector);
    // });

    loadRemoteModule({
      remoteEntry: 'http://localhost:9000/remoteEntry.js',
      remoteName: 'mfe3',
      exposedModule: 'UserComponent',
    }).then(module => {
      this.feedFactorry = this.resolver.resolveComponentFactory(module.UserComponent);
      this.FeedContainer?.createComponent(this.feedFactorry, undefined, this.injector);
    });
  }

  onClick() {
    this.isProfile = !this.isProfile;
    this.isProfile && this.attachComponent();

    !this.isProfile && this.removeComponent();  
    this.changeDetectorRef.detectChanges();
  }

  private attachComponent () {
    this.Member2Container.clear();
    this.MemberContainer?.createComponent(this.memberFactoy, undefined, this.injector);
   
  }
  
  private removeComponent () {
    debugger
      this.MemberContainer.clear();
      this.Member2Container?.createComponent(this.feedFactorry, undefined, this.injector);
  }
}